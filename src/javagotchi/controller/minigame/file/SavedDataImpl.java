package javagotchi.controller.minigame.file;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javagotchi.model.Javagotchi;
import javagotchi.view.minigame.component.GameButton;

/**
 * 
 * @author marica
 *
 */
public final class SavedDataImpl implements SavedData {

    private static final String BESTSCOREPATH = System.getProperty("user.home") + System.getProperty("file.separator")
            + "BestScore.txt";
    private String savedGameFile;
    private Javagotchi javagotchi;

    private static class LazySavedData {
        private static final SavedData SAVEDATA = new SavedDataImpl();
    }

    /**
     * Constructor for the SavedDataImpl.
     */
    private SavedDataImpl() {
    }

    /**
     * Gets SavedData instance.
     * 
     * @return the SavedData instance
     */
    public static SavedData getInstance() {
        return LazySavedData.SAVEDATA;
    }

    @Override
    public Javagotchi getGotchi() {
        return javagotchi;
    }

    @Override
    public void setGotchi(final Javagotchi java) {
        javagotchi = java;
        savedGameFile = System.getProperty("user.home") + System.getProperty("file.separator") + "SaveGame"
                + javagotchi.getInformation().getName() + ".txt";
    }

    @Override
    public String getSaveGame() {
        return savedGameFile;
    }

    @Override
    public boolean existFileSaveGame() {
        return existFile(savedGameFile);
    }

    @Override
    public void writeGame(final Integer score, final List<GameButton> gameButtons, final Integer sec) {
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                new BufferedOutputStream(new FileOutputStream(savedGameFile)))) {
            objectOutputStream.writeObject(score);
            objectOutputStream.writeObject(gameButtons);
            objectOutputStream.writeObject(sec);

            objectOutputStream.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public List<Object> readGameSaved() {
        final List<Object> list = new ArrayList<>();
        boolean check = true;
        try (ObjectInputStream objectInputStream = new ObjectInputStream(
                new BufferedInputStream(new FileInputStream(savedGameFile)))) {
            while (check) {
                try {
                    list.add(objectInputStream.readObject());
                } catch (EOFException e) {
                    check = false;
                }
            }
            objectInputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public void deleteGameSaved() {
        if (existFileSaveGame()) {
            createFile(savedGameFile).delete();
        }
    }

    @Override
    public void deleteGameSaved(final Javagotchi java) {
        this.setGotchi(java);
        deleteGameSaved();
    }

    @Override
    public String getFileBestScore() {
        return BESTSCOREPATH;
    }

    @Override
    public boolean existFileBestScore() {
        return existFile(BESTSCOREPATH);
    }

    private static File createFile(final String path) {
        return new File(path);
    }

    private static boolean existFile(final String path) {
        return createFile(path).exists();
    }

    @Override
    public Map<String, Integer> readBestScore() {
        final Map<String, Integer> map = new LinkedHashMap<>();
        try (DataInputStream dstream = new DataInputStream(new FileInputStream(BESTSCOREPATH))) {
            while (dstream.available() > 0) {
                map.put(dstream.readUTF(), dstream.readInt());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return map.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

    @Override
    public void writeBestScore(final Map<String, Integer> bestScoreMap) {
        try (DataOutputStream dstream = new DataOutputStream(new FileOutputStream(BESTSCOREPATH))) {
            bestScoreMap.entrySet().forEach(e -> {
                try {
                    dstream.writeUTF(e.getKey());
                    dstream.writeInt(e.getValue());
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            });
            dstream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
