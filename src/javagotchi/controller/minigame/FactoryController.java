package javagotchi.controller.minigame;

import javagotchi.controller.minigame.audio.Music;
import javagotchi.controller.minigame.file.SavedData;

/**
 * 
 * @author marica
 *
 */
public interface FactoryController {
    /**
     * Gets ControllerMiniGame.
     * 
     * @return Factory ControllerMiniGame
     */
    ControllerMiniGame getControllerMiniGame();

    /**
     * Gets Music instance.
     * 
     * @return the Music instance
     */
    Music getMusic();

    /**
     * Gets SavedData instance.
     * 
     * @return SavedData instance
     */
    SavedData getSavedData();
}
