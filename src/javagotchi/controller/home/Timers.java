package javagotchi.controller.home;
/**
 * Timers interface. It includes methods to start and stop them.
 * @author elisa
 *
 */
public interface Timers {

    /**
     * Method to start the timers.
     */
    void start();

    /**
     * Method to stop the timers.
     */
    void stop();
}
