package javagotchi.model.minigame;

/**
 * 
 * @author marica
 *
 */
public interface MiniModel {

    /**
     * 
     * @author marica
     *
     */
    enum GameState {
        NULL, START, OVER, PAUSE;
    }

    /**
     * Gets GameGrid.
     * 
     * @return {@link GameGrid}
     */
    GameGrid getGameGrid();

    /**
     * Gets Time.
     * 
     * @return {@link Time}
     */
    Time getTime();

    /**
     * Sets Time.
     * 
     * @param sec
     *            the current second
     */
    void setTime(Integer sec);

    /**
     * Gets Score.
     * 
     * @return {@link Score}
     */
    Score getScore();

    /**
     * Sets Score.
     * 
     * @param score
     *            the current score
     */
    void setScore(Integer score);

    /**
     * Sets the game state.
     * 
     * @param state
     *            {@link GameState}
     */
    void setGameState(GameState state);

    /**
     * Indicates if current game state is equals state.
     * 
     * @param state
     *            {@link GameState}
     * @return true if current game state is equals state
     */
    boolean isGameState(GameState state);

    /**
     * Initializes the model components.
     */
    void initModel();

    /**
     * Indicates if it is the moment to add time.
     * 
     * @return true if it is the moment to add time
     */
    boolean isMomentToAddTime();
}
