package javagotchi.model.information;
/**
 * 
 * @author giulia
 *
 */
public enum Gender {
    /**
     * Javagotchi's gender is female.
     */
    FEMALE, 
    /**
     * Javagotchi's gender is male.
     */
    MALE
}
